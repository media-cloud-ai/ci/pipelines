# Pipelines

Description of all CI pipelines for MCAI projects.

# Runner configuration

You can add a private runner to the project. The runner should be configured as follows:

```toml
[[runners]]
  name = xxxxxxxxxxx
  url = "https://gitlab.com"
  id = xxxxxxxxxxx
  token = xxxxxxxxxx
  token_obtained_at = xxxxxxxxxxx
  token_expires_at = xxxxxxxxxxx
  executor = "docker"
  environment = ["DOCKER_TLS_CERTDIR=/certs","DOCKER_DRIVER=overlay2"]
  limit = 3
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:20.10.18"
    privileged = true
    disable_cache = false
    volumes = ["/certs/client", "/cache"]
    shm_size = 0
  [[runners.docker.services]]
    name = "docker:20.10.18-dind"
    alias = "dinde"
  [runners.variables]
    DOCKER_TLS_CERTDIR = "/certs"
```
